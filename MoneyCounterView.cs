using Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class MoneyCounterView : MonoBehaviour, IMoneyCounterView
    {
        [SerializeField] private Text _counter;
        private Animator _animator;
        private static readonly int _pulse = Animator.StringToHash("Pulse");

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void RenderMoney(float money)
        {
            _counter.text = MoneyHelper.Convert(money);
        }

        public void PulseAnimation(bool active)
        {
            _animator.SetBool(_pulse, active);
        }
    }
    
    public interface IMoneyCounterView
    {
        void RenderMoney(float money);
        void PulseAnimation(bool active);
    }
}
