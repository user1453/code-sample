using App.Services;
using Signals;
using Views;
using Zenject;

namespace App.Presenters
{
    public class MoneyCounterPresenter
    {
        private readonly IMoneyCounterView _counterView;
        private readonly SignalBus _signalBus;

        public MoneyCounterPresenter(MoneyService moneyService, IMoneyCounterView counterView, SignalBus signalBus)
        {
            _signalBus = signalBus;
            _counterView = counterView;

            _signalBus.Subscribe<MoneyChangedSignal>(OnUpdateMoney);
            _signalBus.Subscribe<MoneyPulseAnimationSignal>(signal => { _counterView.PulseAnimation(signal.Active); });
            _counterView.RenderMoney(moneyService.Amount);
        }

        private void OnUpdateMoney(MoneyChangedSignal signal)
        {
            _counterView.RenderMoney(signal.Money);
        }
    }
}