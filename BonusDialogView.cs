using System;
using Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Views
{
    public class BonusDialogView : MonoBehaviour
    {
        public Action Success { get; set; }
        public Action Failure { get; set; }
        [SerializeField] private Button _success;
        [SerializeField] private Button _failure;

        private void Start()
        {
            _success.onClick.AddListener(() =>
            {
                Success?.Invoke();
            });
            _failure.onClick.AddListener(() =>
            {
                Failure?.Invoke();
            });    
        }


        public void Show()
        {
            transform.SetAsLastSibling();
            gameObject.SetActive(true);
        }    

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}