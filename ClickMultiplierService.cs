using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Models;
using Signals;
using UnityEngine;
using Zenject;

namespace App.Services.Bonuses
{
    public class ClickMultiplierService
    {
        public float Multiplier = 1;
        private List<Multiplier> _multipliers = new List<Multiplier>();

        private ClickMultiplierService(SignalBus signalBus, AsyncProcessor asyncProcessor)
        {
            signalBus.Subscribe<AddClickMultiplierSignal>(AddMultiplier);
            asyncProcessor.StartCoroutine(EverySecond());
        }

        private float GetMultiplier()
        {
            if (_multipliers.Count == 0)
                return 1;

            return _multipliers.Aggregate(1f, (total, next) => total * next.Value);
        }


        private IEnumerator EverySecond()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(1);

                if (_multipliers.Count > 0)
                    ClearExpiredMultipliers();
                Multiplier = GetMultiplier();
            }
        }

        private void ClearExpiredMultipliers()
        {
            _multipliers = _multipliers.Where(x => x.ExpirationTime > DateTime.Now).ToList();
        }

        private void AddMultiplier(AddClickMultiplierSignal signal)
        {
            _multipliers.Add(new Multiplier(signal.Multiplier, DateTime.Now.AddMinutes(signal.Minutes)));
        }
    }
}