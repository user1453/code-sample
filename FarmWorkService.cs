using System;
using App.Services.Bonuses;
using App.Systems;
using Signals;
using Zenject;

namespace App.Services
{
    public class FarmWorkService
    {
        private readonly SignalBus _signalBus;
        private readonly SpeedMultiplierService _speedMultiplierService;

        private  FarmWorkService(SpeedMultiplierService speedMultiplierService, SignalBus signalBus)
        {
            _speedMultiplierService = speedMultiplierService;
            _signalBus = signalBus;
        }

        public void WorkIdle(IProduction farm)
        {
            if (farm.CurrentVolume < farm.MaxVolume)
            {
                farm.Progress += farm.Speed * _speedMultiplierService.Multipler * .1f ;
                Calculate(farm);
            }

            _signalBus.Fire(new ResourcesUpdateOnFarmSignal(farm));
        }

        public void WorkRestore(IProduction farm, float seconds)
        {
            if (farm.CurrentVolume < farm.MaxVolume)
            {
                farm.Progress += farm.Speed * seconds;
                Calculate(farm);
            }

            _signalBus.Fire(new ResourcesUpdateOnFarmSignal(farm));
        }


        public void WorkClick(IProduction farm, float multiplier)
        {
            if (farm.CurrentVolume < farm.MaxVolume)
            {
                farm.Progress += farm.Speed * multiplier * farm.ClickLevel;
                Calculate(farm);
            }

            _signalBus.Fire(new ResourcesUpdateOnFarmSignal(farm));
        }

        private void Calculate(IProduction farm)
        {
            if (farm.Progress > farm.ProductionRate)
            {
                var truncate = (float) Math.Truncate(farm.Progress);
                farm.Progress -= truncate;
                farm.CurrentVolume += truncate;
            }

            if (farm.CurrentVolume > farm.MaxVolume)
            {
                farm.CurrentVolume = farm.MaxVolume;
                farm.Progress = 0;
            }
        }
    }
}